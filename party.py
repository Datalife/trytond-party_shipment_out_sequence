# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields, ModelSQL
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Id
from trytond.tools.multivalue import migrate_property
from trytond.modules.company.model import CompanyValueMixin


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    shipment_out_sequence = fields.MultiValue(
        fields.Many2One('ir.sequence', 'Customer Shipment Sequence', domain=[
            ('company', 'in', [Eval('context', {}).get('company', -1), None]),
            ('sequence_type', '=', Id('stock', 'sequence_type_shipment_out'))])
    )
    shipment_out_sequences = fields.One2Many(
        'party.party.shipment_out_sequence', 'party',
        "Shipment Out Sequences")

    @classmethod
    def default_shipment_out_sequence(cls, **pattern):
        return cls.multivalue_model(
            'shipment_out_sequence').default_shipment_out_sequence()


class PartyShipmentOutSequence(ModelSQL, CompanyValueMixin):
    """Party Sequence"""
    __name__ = 'party.party.shipment_out_sequence'

    party = fields.Many2One('party.party', 'Party', select=True,
        ondelete='CASCADE')
    shipment_out_sequence = fields.Many2One('ir.sequence',
        'Customer Shipment Sequence',
        domain=[
            ('company', 'in', [Eval('context', {}).get(
                'company', -1), None]),
            ('sequence_type', '=', Id('stock', 'sequence_type_shipment_out'))])

    @classmethod
    def __register__(cls, module_name):
        table_h = cls.__table_handler__(module_name)
        exist = table_h.table_exist(cls._table)

        super(PartyShipmentOutSequence, cls).__register__(module_name)

        if not exist:
            cls._migrate_property([], [], [])

    @classmethod
    def _migrate_property(cls, field_names, value_names, fields):
        field_names.append('shipment_out_sequence')
        value_names.append('shipment_out_sequence')
        fields.append('company')
        migrate_property('party.party', field_names, cls, value_names,
            parent='party', fields=fields)

    @classmethod
    def default_shipment_out_sequence(cls):
        pool = Pool()
        Config = pool.get('stock.configuration')

        return Config(1).shipment_out_sequence.id


class ShipmentOut(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out'

    sequence = fields.Many2One('ir.sequence', 'Sequence',
        select=True, ondelete='CASCADE', domain=[
            ('company', 'in', [Eval('context', {}).get('company', -1), None]),
            ('sequence_type', '=', Id('stock', 'sequence_type_shipment_out'))],
        states={
            'readonly': ((Eval('state') != 'draft') |
                Eval('outgoing_moves', [0]) |
                Eval('number')),
            }, required=True,
        depends=['state', 'number', 'outgoing_moves'])

    @staticmethod
    def default_sequence():
        pool = Pool()
        Config = pool.get('stock.configuration')

        return Config(1).shipment_out_sequence.id

    @classmethod
    def create(cls, vlist):
        pool = Pool()
        Sequence = pool.get('ir.sequence')
        Party = pool.get('party.party')
        Conf = pool.get('stock.configuration')

        vlist = [x.copy() for x in vlist]
        config = Conf(1)
        default_company = cls.default_company()
        for values in vlist:
            if not values.get('sequence', None):
                party = Party(values['customer'])
                sequence = (
                    party.get_multivalue(
                        'shipment_out_sequence',
                        company=values.get('company', default_company))
                    or config.get_multivalue(
                        'shipment_out_sequence',
                        company=values.get('company', default_company)))
                values['sequence'] = sequence.id
            if values.get('number', None) is None:
                values['number'] = Sequence(values['sequence']).get()
        return super(ShipmentOut, cls).create(vlist)

    @fields.depends('customer')
    def on_change_customer(self):
        super(ShipmentOut, self).on_change_customer()
        if self.customer and self.customer.shipment_out_sequence:
            self.sequence = self.customer.shipment_out_sequence


class ShipmentOutReturn(metaclass=PoolMeta):
    __name__ = 'stock.shipment.out.return'

    sequence = fields.Many2One('ir.sequence', 'Sequence',
        select=True, ondelete='CASCADE', domain=[
            ('company', 'in', [Eval('context', {}).get('company', -1), None]),
            ('sequence_type', '=', Id('stock',
                'sequence_type_shipment_out_return'))],
        states={
            'readonly': ((Eval('state') != 'draft') |
                Eval('incoming_moves', [0]) |
                Eval('number')),
            }, required=True,
        depends=['state', 'number', 'incoming_moves'])

    @staticmethod
    def default_sequence():
        pool = Pool()
        Config = pool.get('stock.configuration')

        return Config(1).shipment_out_return_sequence.id
