# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from .party import (Party, ShipmentOut, ShipmentOutReturn,
    PartyShipmentOutSequence)


def register():
    Pool.register(
        Party,
        PartyShipmentOutSequence,
        ShipmentOut,
        ShipmentOutReturn,
        module='party_shipment_out_sequence', type_='model')
